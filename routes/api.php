<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\UserAuthController;
use App\Http\Controllers\ProduitsController ;
use App\Http\Controllers\MarchandiseController;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/* 
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
}); */
Route::post('/register', [ProduitsController::class, 'register']);
Route::get('/test', [ProduitsController::class, 'index']);
Route::post('/login', [ProduitsController::class, 'login']);
Route::get('/get-marchandise', [MarchandiseController::class, 'getAll']);
Route::post('/get-one-marchandise', [MarchandiseController::class, 'getOne']);
Route::post('/create', [MarchandiseController::class, 'createMarchandise']);
Route::post('/delete-marchandise', [MarchandiseController::class, 'delete']);
Route::post('/update-marchandise', [MarchandiseController::class, 'updateMarchandise']);
Route::get('/transaction', [MarchandiseController::class, 'transactions']);

//Route::post('/register', 'Auth\UserAuthController@register');
//Route::post('/login', 'Auth\UserAuthController@login');

Route::apiResource('/employee', 'EmployeeController')->middleware('auth:api');


